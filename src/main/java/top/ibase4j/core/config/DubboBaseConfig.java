package top.ibase4j.core.config;

import org.springframework.context.annotation.Bean;

import com.alibaba.dubbo.config.ApplicationConfig;
import com.alibaba.dubbo.config.RegistryConfig;

import top.ibase4j.core.util.PropertiesUtil;

/**
 * @author ShenHuaJie
 * @since 2019年4月4日 下午2:58:39
 */
public class DubboBaseConfig {
    @Bean
    public ApplicationConfig application() {
        ApplicationConfig applicationConfig = new ApplicationConfig();
        applicationConfig.setQosEnable(true);
        applicationConfig.setQosAcceptForeignIp(false);
        applicationConfig.setQosPort(PropertiesUtil.getInt("rpc.protocol.port", 22222) + 10);
        applicationConfig.setName(PropertiesUtil.getString("rpc.registry.name"));
        applicationConfig.setLogger("slf4j");
        return applicationConfig;
    }

    @Bean
    public RegistryConfig registry() {
        RegistryConfig registryConfig = new RegistryConfig();
        String rpcAddress = PropertiesUtil.getString("rpc.address");
        registryConfig.setRegister(!rpcAddress.contains("localhost") && !rpcAddress.contains("127.0.0.1"));
        registryConfig.setAddress(rpcAddress);
        registryConfig.setProtocol(PropertiesUtil.getString("rpc.registry"));
        registryConfig.setFile(PropertiesUtil.getString("rpc.cache.dir") + "/dubbo-"
                + PropertiesUtil.getString("rpc.registry.name") + ".cache");
        return registryConfig;
    }

}
